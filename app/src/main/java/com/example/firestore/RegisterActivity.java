package com.example.firestore;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class RegisterActivity extends AppCompatActivity {
    EditText nama,email,pass;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nama =  findViewById(R.id.edNameRegist);
        email = findViewById(R.id.edEmailRegist);
        pass = findViewById(R.id.edPassRegist);


    }

    public void regist(View view){
        if (check()){
            mAuth = FirebaseAuth.getInstance();
            mAuth.createUserWithEmailAndPassword(email.getText().toString(),pass.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                FirebaseUser user = mAuth.getCurrentUser();
                                UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(nama.getText().toString()).build();

                                user.updateProfile(userProfileChangeRequest);
                                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                                finish();
                            }
                        }
                    });
        }
    }


    public boolean check(){
        if (nama.getText().toString().equals("")){
            nama.setError("Masukkan Namamu");
            nama.requestFocus();
            return false;
        }
        if (email.getText().toString().equals("")){
            email.setError("Masukkan Namamu");
            email.requestFocus();
            return false;
        }

        if (pass.getText().toString().equals("")){
            pass.setError("Masukkan Namamu");
            pass.requestFocus();
            return false;
        }
        return true;
    }


    public void gotoLogin(View view){
        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
        finish();
    }
}
